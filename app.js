const path = require('path')
const mysql = require('mysql')
const csv = require('csvtojson')
const dotenv = require('dotenv')

dotenv.config()

const matchesFilePath = path.join(__dirname, 'data', 'matches.csv')
const deliveriesFilePath = path.join(__dirname, 'data', 'deliveries.csv')

const connection = mysql.createConnection({
  host: process.env.DB_HOST,
  user: process.env.DB_USER,
  password: process.env.DB_PASSWORD,
  database: process.env.DB_DATABASE,
})

csv()
  .fromFile(matchesFilePath)
  .then((res) => {
    let data = res.map((element) => {
      let result = Object.values(element)
      return result
    })

    connection.query(
      'insert into matches values ?',
      [data],
      function (error, results) {
        if (error) throw error
        console.log(results)
      }
    )
  })
  .catch((error) => console.error(error))

csv()
  .fromFile(deliveriesFilePath)
  .then((res) => {
    let data = res.map((element) => {
      let result = Object.values(element)
      return result
    })

    connection.query(
      'insert into deliveries values ?',
      [data],
      function (error, results) {
        if (error) throw error
        console.log(results)
      }
    )
  })
  .catch((error) => console.error(error))
